﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" Inherits="SiteMap" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Site Map</h2>
</asp:Content>

<asp:Content ID="BodySec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <asp:TreeView ID="tvSiteMap" runat="server" DataSourceID="mdsSiteMap"></asp:TreeView>

    <asp:SiteMapDataSource runat="server" ID="mdsSiteMap"></asp:SiteMapDataSource>
</asp:Content>

