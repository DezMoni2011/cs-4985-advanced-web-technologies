﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the UpdateFeedback page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 19, 2015 | Spring
/// </version>
public partial class UpdateFeedback : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
    }
    /// <summary>
    /// Handles the RowUpdated event of the gvUpdatedFeedbacks control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdatedEventArgs" /> instance containing the event data.</param>
    protected void gvUpdatedFeedbacks_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text =
                "Database error has occured. Please refresh page or resolve confilicting operation. Hit cancel to quit<br/><br/> " +
                "Message: " + e.Exception.Message;

            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else
        {
            this.lblErrorMessage.Text = "";
        }
    }
    /// <summary>
    /// Handles the Updated event of the odsUpdatedFeedbacks control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void odsUpdatedFeedbacks_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
       e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }
}