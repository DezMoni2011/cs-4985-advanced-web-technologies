﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the IncidentDisplay page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 4, 2015 | Spring
/// </version>
public partial class IncidentDisplay : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
        if (this.ddlCustomers.SelectedItem != null)
        {
            this.lblCustomer.Text = this.ddlCustomers.SelectedItem.Text;
        }
        
    }
    /// <summary>
    /// Handles the Click event of the btnViewSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnViewSoftware_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }
    /// <summary>
    /// Handles the Click event of the btnManageCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnManageCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerMaintenance.aspx");
    }
}