﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="FeedbackBySupport.aspx.cs" Inherits="FeedbackBySupport" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Support.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="titleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Unresolved Issues</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label class="box">Staff Memeber: </label>
    <asp:DropDownList ID="ddlSupport" runat="server"
        DataSourceID="obsSupport"
        DataTextField="Name"
        DataValueField="SupportID"
        AutoPostBack="True" CssClass="box">
    </asp:DropDownList><br /><br />
    <asp:ObjectDataSource ID="obsSupport" runat="server" 
        SelectMethod="GetAllSupportStaff" 
        TypeName="SupportDatabase">
    </asp:ObjectDataSource>

    <asp:GridView ID="gvSupportFeedback" runat="server"
        DataSourceID="obsSupportFeedback"
        BackColor="White"
        BorderColor="#E7E7FF"
        BorderStyle="None"
        BorderWidth="1px"
        CellPadding="3"
        GridLines="Horizontal"
        CssClass="SupportFeedback">
        <AlternatingRowStyle BackColor="#F7F7F7"></AlternatingRowStyle>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>

        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>

        <PagerStyle HorizontalAlign="Right" BackColor="#E7E7FF" ForeColor="#4A3C8C"></PagerStyle>

        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"></RowStyle>

        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedRowStyle>

        <SortedAscendingCellStyle BackColor="#F4F4FD"></SortedAscendingCellStyle>

        <SortedAscendingHeaderStyle BackColor="#5A4C9D"></SortedAscendingHeaderStyle>

        <SortedDescendingCellStyle BackColor="#D8D8F0"></SortedDescendingCellStyle>

        <SortedDescendingHeaderStyle BackColor="#3E3277"></SortedDescendingHeaderStyle>
    </asp:GridView>
    <asp:ObjectDataSource ID="obsSupportFeedback" runat="server" SelectMethod="GetOpenFeedbackIncidents" TypeName="FeedbackDatabase">
        <SelectParameters>
            <asp:ControlParameter 
                ControlID="ddlSupport" 
                PropertyName="SelectedValue" 
                Name="supportStaffId" 
                Type="Int32">
            </asp:ControlParameter>
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">

</asp:Content>

