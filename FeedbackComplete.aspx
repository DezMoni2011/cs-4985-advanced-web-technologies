﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Feedback Confirmation</h2>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="bodyContent" Runat="Server">
    <h3>Sumbmitted information...</h3>

    <p>Submission for Customer ID: <asp:Label ID="lblCustomerId" runat="server" Text=""></asp:Label> is being processed.</p>
    
    
    <p>
        We at The Digital Upscale Maanger of Ballgames would like to thank you for your feedback.<br/>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label><br/>
        Keep managing with us!
    </p>

    <asp:Button ID="btnCreateFeedback" runat="server" Text="New Feedback Request" OnClick="btnCreateFeedback_Click" />
    
</asp:Content>

