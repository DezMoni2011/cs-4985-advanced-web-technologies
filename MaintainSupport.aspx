﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="MaintainSupport.aspx.cs" Inherits="MaintainSupport" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Support.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="titleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Support Maintenance</h2>
</asp:Content>
<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label class="box"> Staff Memeber: </label>
    <asp:DropDownList ID="ddlSupport" CssClass="box" runat="server" 
        DataSourceID="sdsSupport" 
        DataTextField="Name" 
        DataValueField="SupportID" AutoPostBack="True">
    </asp:DropDownList><br/><br />

    <asp:SqlDataSource runat="server" ID="sdsSupport" 
        ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>' 
        ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>' 
        SelectCommand="SELECT [SupportID], [Name] 
            FROM [Support] 
            ORDER BY [Name]">
    </asp:SqlDataSource>
    
    <h3>Staff Memeber Information</h3><br/>
    <asp:FormView ID="fvSupportDetails" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="SupportID" DataSourceID="sdsSupportView" GridLines="Horizontal" OnItemDeleted="fvSupportDetails_ItemDeleted" OnItemInserted="fvSupportDetails_ItemInserted" OnItemUpdated="fvSupportDetails_ItemUpdated">
        <EditItemTemplate>
            <table>
                <tr>
                    <td class="column1">
                       ID:
                    </td>
                    <td class="column2">
                        <asp:Label ID="SupportIDLabel1" runat="server" Text='<%# Eval("SupportID") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Name:
                    </td>
                    <td class="column2">
                         <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' CssClass="box" />
                         <asp:RequiredFieldValidator ID="rfvEditName" runat="server" ControlToValidate="NameTextBox" Display="Dynamic" ErrorMessage="Name field is required.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Email:
                    </td>
                    <td class="column2">
                        <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' CssClass="box" />
                        <asp:RequiredFieldValidator ID="rfvEditEmail" runat="server" ControlToValidate="EmailTextBox" Display="Dynamic" ErrorMessage="Email field is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEditEmail" runat="server" ControlToValidate="EmailTextBox" Display="Dynamic" ErrorMessage="Must be in the form of email@host.com" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Phone:
                    </td>
                    <td class="column2">
                         <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' CssClass="box" />
                         <asp:RequiredFieldValidator ID="rfvEditPhone" runat="server" ControlToValidate="PhoneTextBox" Display="Dynamic" ErrorMessage="Phone field is required.">*</asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="revEditPhone" runat="server" ControlToValidate="PhoneTextBox" Display="Dynamic" ErrorMessage="Must be in the form of 888-888-8888" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <InsertItemTemplate>
            <table>
                <tr>
                    <td class="column1">
                        ID:
                    </td>
                    <td class="column2">
                         <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("SupportID") %>' CssClass="box"/>
                         <asp:RequiredFieldValidator ID="rfvInsertID" runat="server" ControlToValidate="IdTextBox" Display="Dynamic" ErrorMessage="ID field is required.">*</asp:RequiredFieldValidator>
                         <asp:CompareValidator ID="cvInsertID" runat="server" ControlToValidate="IdTextBox" Display="Dynamic" ErrorMessage="Only Intergers are allowed for the ID field." Operator="GreaterThan" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Name:
                    </td>
                    <td class="column2">
                        <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' CssClass="box" />
                        <asp:RequiredFieldValidator ID="rfvInsertName" runat="server" ControlToValidate="NameTextBox" Display="Dynamic" ErrorMessage="Name field  is required.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Email:
                    </td>
                    <td class="column2">
                        <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' CssClass="box" />
                        <asp:RequiredFieldValidator ID="rfvInsertEmail" runat="server" ControlToValidate="EmailTextBox" Display="Dynamic" ErrorMessage="Email field is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revInsertEmail" runat="server" ControlToValidate="EmailTextBox" Display="Dynamic" ErrorMessage="Must be in form of email@host.com" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                       Phone:
                    </td>
                    <td class="column2">
                        <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' CssClass="box" />
                        <asp:RequiredFieldValidator ID="rfvInsertPhone" runat="server" ControlToValidate="PhoneTextBox" Display="Dynamic" ErrorMessage="Phone field is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revInsertPhone" runat="server" ControlToValidate="PhoneTextBox" Display="Dynamic" ErrorMessage="Must be in the form of 888-888-8888" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <table>
                <tr>
                    <td class="column1">
                       ID:
                    </td>
                    <td class="column2">
                        <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Name:
                    </td>
                    <td class="column2">
                         <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                    </td>
                </tr>
                <tr>
                   <td class="column1">
                        Email:
                    </td>
                    <td class="column2">
                        <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="column1">
                        Phone:
                    </td>
                    <td class="column2">
                        <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
        </ItemTemplate>
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
    </asp:FormView>
    <asp:SqlDataSource ID="sdsSupportView" runat="server" ConnectionString="<%$ ConnectionStrings:strDigitalMananger %>" ProviderName="<%$ ConnectionStrings:strDigitalMananger.ProviderName %>" SelectCommand="SELECT * FROM [Support] WHERE ([SupportID] = ?)" DeleteCommand="DELETE FROM [Support] WHERE [SupportID] = ?" InsertCommand="INSERT INTO [Support] ([SupportID], [Name], [Email], [Phone]) VALUES (?, ?, ?, ?)" UpdateCommand="UPDATE [Support] SET [Name] = ?, [Email] = ?, [Phone] = ? WHERE [SupportID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="SupportID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SupportID" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlSupport" Name="SupportID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="SupportID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="ErrorMEssageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
    <asp:Label ID="lblErrorMessage" runat="server" CssClass="validator" Text=""></asp:Label>

    <asp:ValidationSummary ID="vsSupoort" runat="server" HeaderText="Please correct the following issues." />
</asp:Content>

