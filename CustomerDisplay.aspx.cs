﻿using System;
using System.Data;
using System.Web.UI;

/// <summary>
/// Code behind for the CustomerDisplay page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// February 28, 2015 | Spring
/// </version>
public partial class CustomerDisplay : Page
{
    private Customer _selectedCustomer;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
        if (!IsPostBack) this.ddlCustomer.DataBind();
        this._selectedCustomer = this.GetSelectedCustomer();
        this.lblCustomerID.Text = this._selectedCustomer.CustomerId;
        this.lblAddress.Text = this._selectedCustomer.Address;
        this.lblCity.Text = this._selectedCustomer.City;
        this.lblState.Text = this._selectedCustomer.State;
        this.lblZipCode.Text = this._selectedCustomer.ZipCode;
        this.lblPhone.Text = this._selectedCustomer.PhoneNumber;
        this.lblEmail.Text = this._selectedCustomer.Email;
    }

    /// <summary>
    /// Gets the selected customer.
    /// </summary>
    /// <returns>
    /// A customer from the selcted choice.
    /// </returns>
    private Customer GetSelectedCustomer()
    {
        var customerTable = (DataView)this.sqldsCustomer.Select(DataSourceSelectArguments.Empty);
        var customer = new Customer();

        if (customerTable == null)
        {
            return customer;
        }
        customerTable.RowFilter = string.Format("CustomerID = '{0}'", this.ddlCustomer.SelectedValue);
        var row = customerTable[0];

        customer.CustomerId = row["CustomerID"].ToString();
        customer.FullName = row["Name"].ToString();
        customer.Address = row["Address"].ToString();
        customer.City = row["City"].ToString();
        customer.State = row["State"].ToString();
        customer.ZipCode = row["ZipCode"].ToString();
        customer.PhoneNumber = row["Phone"].ToString();
        customer.Email = row["Email"].ToString();

        return customer;
    }

    /// <summary>
    /// Handles the Click event of the btnAddCustomer control.
    /// Adds the chosen customer to the customer list unless that customer is already int that particular list.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAddCustomer_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        var customerList = CustomerList.GetCustomers();

        var customer = customerList[this._selectedCustomer.CustomerId];

        if (customer == null)
        {
            customerList.AddItem(this._selectedCustomer);
            this.lblErrorMessage.Text = "";
            Response.Redirect("ContactList.aspx");
        }
        else
        {
            this.lblErrorMessage.Text = "Customer is already in the list";
        }

    }

    /// <summary>
    /// Handles the Click event of the btnViewCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnViewCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }
}