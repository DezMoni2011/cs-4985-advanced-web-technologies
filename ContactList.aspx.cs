﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the ContactList page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// February 28, 2015 | Spring
/// </version>
public partial class ContactList : Page
{
    /// <summary>
    /// The _customer list
    /// </summary>
    private CustomerList _customerList;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._customerList = CustomerList.GetCustomers();
        this.lblMessage.Text = "";

        if (!IsPostBack)
        {
            this.DisplayCustomers();
        }
    }


    /// <summary>
    /// Displays the customers.
    /// </summary>
    private void DisplayCustomers()
    {
        this.lbCustomerList.Items.Clear();
        this._customerList.Sort();

        for (var i = 0; i < this._customerList.Count; i++)
        {
            var customer = this._customerList[i];
            this.lbCustomerList.Items.Add(customer.Display());
        }

    }

    /// <summary>
    /// Handles the Click event of the btnSelectAdditonalCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSelectAdditonalCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerDisplay.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnRemoveCustomer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnRemoveCustomer_Click(object sender, EventArgs e)
    {
        if (this._customerList.Count <= 0)
        {
            this.lblMessage.Text = "There are no customers to remove.";
            return;
        }
        if (this.lbCustomerList.SelectedIndex > -1)
        {
            this._customerList.RemoveAt(this.lbCustomerList.SelectedIndex);
            this.DisplayCustomers();
            this.lblMessage.Text = "";
        }
        else
        {
            this.lblMessage.Text = "Please select an item to remove";
        }

    }

    /// <summary>
    /// Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.lblMessage.Text = "";

        if (this._customerList.Count <= 0)
        {
            this.lblMessage.Text = "The list is clear.";
            return;
        }
        this._customerList.Clear();
        this.lbCustomerList.Items.Clear();
    }
}