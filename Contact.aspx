﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/ContactUs.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="titleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Contact Us!</h2>
</asp:Content>

<asp:Content ID="bodySec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label>Phone:</label>
    <p>404-555-6893</p>

    <label>Hours of Operation:</label>
    <p>
        Monday - Friday 9am - 5pm
    </p>

    <label>Email:</label>
    <a href="mailto:digitalBallgameManager@tommycopper.com">digitalBallgamManager@tommycopper.com</a>

    <label>Mailing Address:</label>
    <p>
        915 Lovvorn Rd<br/>
        Carrollton, GA 
        30117
    </p>

</asp:Content>

