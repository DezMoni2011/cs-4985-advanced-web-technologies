﻿using System;
using System.Diagnostics;

/// <summary>
/// Respresents a Customer object.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Feburary 5, 2015 | Spring
/// </version>
public class Customer : IComparable<Customer>
{
    /// <summary>
    /// The _id
    /// </summary>
    private String _id;
    /// <summary>
    /// The _full name
    /// </summary>
    private String _fullName;
    /// <summary>
    /// The _address
    /// </summary>
    private String _address;
    /// <summary>
    /// The _city
    /// </summary>
    private String _city;
    /// <summary>
    /// The _state
    /// </summary>
    private String _state;
    /// <summary>
    /// The _zip code
    /// </summary>
    private String _zipCode;
    /// <summary>
    /// The _phone number
    /// </summary>
    private String _phoneNumber;
    /// <summary>
    /// The _email
    /// </summary>
    private String _email;

    /// <summary>
    /// Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public String CustomerId
    {
        get { return this._id; }
        set
        {
            Trace.Assert(value != null, "ID must not be null");
            this._id = value;
        }
    }

    /// <summary>
    /// Gets or sets the full name.
    /// </summary>
    /// <value>
    /// The full name.
    /// </value>
    public String FullName
    {
        get { return this._fullName; }
        set
        {
            Trace.Assert(value != null,"Full name must not be null");
            this._fullName = value;
        }
    }

    /// <summary>
    /// Gets or sets the address.
    /// </summary>
    /// <value>
    /// The address.
    /// </value>
    public String Address
    {
        get { return this._address; }
        set
        {
            Trace.Assert(value != null, "Address must not be null");
            this._address = value;
        }
    }

    /// <summary>
    /// Gets or sets the city.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    public String City
    {
        get { return this._city; }
        set
        {
            Trace.Assert(value != null, "City must not be null");
            this._city = value;
        }
    }

    /// <summary>
    /// Gets or sets the state.
    /// </summary>
    /// <value>
    /// The state.
    /// </value>
    public String State
    {
        get { return this._state; }
        set
        {
            Trace.Assert(value != null, "State must not be null");
            this._state = value;
        }
    }

    /// <summary>
    /// Gets or sets the zip code.
    /// </summary>
    /// <value>
    /// The zip code.
    /// </value>
    public String ZipCode
    {
        get { return this._zipCode; }
        set
        {
            Trace.Assert(value != null, "ZipCode must not be null");
            this._zipCode = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone number.
    /// </summary>
    /// <value>
    /// The phone number.
    /// </value>
    public String PhoneNumber
    {
        get { return this._phoneNumber; }
        set
        {
            Trace.Assert(value != null, "Phone Number must not be null");
            this._phoneNumber = value;
        }
    }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    public String Email
    {
        get { return this._email; }
        set
        {
            Trace.Assert(value != null, "Email must not be null");
            this._email = value;
        }
    }

    /// <summary>
    /// Displays this instance.
    /// </summary>
    /// <returns>
    /// String representation of info.
    /// </returns>
    public String Display()
    {
        var firstName = this._fullName.Split(' ')[0];
        var lastName = this._fullName.Split(' ')[1];

        return lastName + ", " + firstName + ": " + this._phoneNumber + "; " + this._email;
    }

    public int CompareTo(Customer other)
    {
        var thisName = this.FullName.Split();
        var otherName = other.FullName.Split();

        var thisLastName = thisName[1];
        var otherLastName = otherName[1];

        return String.Compare(thisLastName, otherLastName, 0);
    }
}