﻿using System.Configuration;

/// <summary>
/// Connection to ballgame database.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 19, 2015 |Spring
/// </version>
public class BallgameDatabase
{
    /// <summary>
    /// Gets the connection string.
    /// </summary>
    /// <returns></returns>
    public static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["strDigitalMananger"].ConnectionString;
    }
}