﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Retireves information related to the Support table of the database.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 19, 2015 | Spring
/// </version>
public class SupportDatabase
{
    /// <summary>
    /// Gets all support staff.
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetAllSupportStaff()
    {
        var database = new OleDbConnection(BallgameDatabase.GetConnectionString());

        const string selectStatement = "SELECT SupportID, Name " +
                                        "FROM Support " +
                                        "ORDER BY Name";

        var command = new OleDbCommand(selectStatement, database);
        database.Open();

        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }
}