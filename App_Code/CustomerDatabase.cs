﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Summary description for CustomerDatabase
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 19, 2015 | Spring
/// </version>
public class CustomerDatabase
{
    /// <summary>
    /// Gets the customers with feedback.
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetCustomersWithFeedback()
    {
        var database = new OleDbConnection(BallgameDatabase.GetConnectionString());

        const string distinctSelectStatement = "SELECT CustomerID, Name " +
                                               "FROM Customer " +
                                               "WHERE CustomerID " +
                                                "IN " +
                                                    "(SELECT DISTINCT CustomerID " +
                                                      "FROM Feedback " +
                                                      "WHERE SupportID IS NOT NULL) " +
                                               "ORDER By Name";
        var command = new OleDbCommand(distinctSelectStatement, database);
        database.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }
}