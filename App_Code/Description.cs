﻿using System;
using System.Diagnostics;

/// <summary>
/// Represents a description object.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 5, 2015 | Spring
/// </version>
public class Description
{
    /// <summary>
    /// The _customer identifier
    /// </summary>
    private int _customerId;
    /// <summary>
    /// The _feedback identifier
    /// </summary>
    private int _feedbackId;
    /// <summary>
    /// The _service time
    /// </summary>
    private int _serviceTime;
    /// <summary>
    /// The _efficiency
    /// </summary>
    private int _efficiency;
    /// <summary>
    /// The _resolution
    /// </summary>
    private int _resolution;
    /// <summary>
    /// The _comments
    /// </summary>
    private String _comments;

    /// <summary>
    /// The _contact method
    /// </summary>
    private String _contactMethod;

    /// <summary>
    /// Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public int CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value >= 0, "CustomerId must not be null.");
            this._customerId = value;
        }
    }

    /// <summary>
    /// Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    /// The feedback identifier.
    /// </value>
    public int FeedbackId
    {
        get { return this._feedbackId; }
        set
        {
            Trace.Assert(value >= 0, "FeedbackId must not be null.");
            this._feedbackId = value;
        }
    }

    /// <summary>
    /// Gets or sets the service time.
    /// </summary>
    /// <value>
    /// The service time.
    /// </value>
    public int ServiceTime
    {
        get { return this._serviceTime; }
        set
        {
            Trace.Assert(value >= 0, "ServiceTime must not be null.");
            this._serviceTime = value;
        }
    }

    /// <summary>
    /// Gets or sets the efficiency.
    /// </summary>
    /// <value>
    /// The efficiency.
    /// </value>
    public int Efficiency
    {
        get { return this._efficiency; }
        set
        {
            Trace.Assert(value >= 0, "Efficiency must not be null.");
            this._efficiency = value;
        }
    }

    /// <summary>
    /// Gets or sets the resolution.
    /// </summary>
    /// <value>
    /// The resolution.
    /// </value>
    public int Resolution
    {
        get { return this._resolution; }
        set
        {
            Trace.Assert(value >= 0, "Resolution must not be null.");
            this._resolution = value;
        }
    }

    /// <summary>
    /// Gets or sets the comments.
    /// </summary>
    /// <value>
    /// The comments.
    /// </value>
    public String Comments
    {
        get { return this._comments; }
        set
        {
            Trace.Assert(value != null, "Comments must not be null.");
            this._comments = value;
        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Description"/> is contact.
    /// </summary>
    /// <value>
    ///   <c>true</c> if contact; otherwise, <c>false</c>.
    /// </value>
    public Boolean Contact { get; set; }

    /// <summary>
    /// Gets or sets the contact method.
    /// </summary>
    /// <value>
    /// The contact method.
    /// </value>
    public String ContactMethod
    {
        get { return this._contactMethod; }
        set
        {
            Trace.Assert(value != null, "Contact must not be null.");
            this._contactMethod = value;
        }
    }
}