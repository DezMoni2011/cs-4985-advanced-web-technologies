﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Retireves information related to the Feedback table of the database.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 19, 2015 | Spring
/// </version>
public class FeedbackDatabase
{
    /// <summary>
    /// Gets the open feedback incidents.
    /// </summary>
    /// <param name="supportStaffId">The support staff identifier.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetOpenFeedbackIncidents(int supportStaffId)
    {
        var database = new OleDbConnection(BallgameDatabase.GetConnectionString());

        var selectStatment = "SELECT Feedback.DateOpened AS [Date Opened], " +
                             "Software.Name AS Software, " +
                             "Customer.Name AS Customer " +
                             "FROM (" +
                             "(Feedback INNER JOIN Software ON Feedback.SoftwareID = Software.SoftwareID) " +
                             "INNER JOIN Customer ON Feedback.CustomerID = Customer.CustomerID) " +
                             "WHERE (Feedback.SupportID = " + supportStaffId + " AND Feedback.DateClosed IS NULL)";

        var command = new OleDbCommand(selectStatment, database);
        command.Parameters.AddWithValue("SupportID", supportStaffId);
        database.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    /// Gets the customer feedback.
    /// </summary>
    /// <param name="customerId">The customer identifier.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetCustomerFeedback(int customerId)
    {
        var database = new OleDbConnection(BallgameDatabase.GetConnectionString());

        var selectStatement = "SELECT SoftwareID, SupportID, " +
                                "DateOpened, DateClosed, Title, Description " +
                              "FROM Feedback " +
                              "WHERE CustomerID = " + customerId + " " +
                              "ORDER BY SupportID";
        var command = new OleDbCommand(selectStatement, database);
        database.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    /// Updates the feedback.
    /// </summary>
    /// <param name="originalFeedback">The original feedback.</param>
    /// <param name="newFeedback">The new feedback.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int UpdateFeedback(Feedback originalFeedback,
        Feedback newFeedback)
    {
        var database = new OleDbConnection(BallgameDatabase.GetConnectionString());

        const string updateStatement = "UPDATE Feedback " +
                              "SET DateClosed = @DateClosed, " +
                                  "Description = @Description " +
                              "WHERE FeedbackID = @original_Feedback " +
                                       "AND DateClosed = @original_DateClosed " +
                                       "AND Description = @original_Description";

        var command = new OleDbCommand(updateStatement, database);

        var date = Convert.ToDateTime(newFeedback.DateClosed);

        if (date == Convert.ToDateTime("01/01/0001 12:00:00 AM"))
        {
           command.Parameters.AddWithValue("DateClosed",
           DBNull.Value);
        }
        else
        {
            command.Parameters.AddWithValue("DateClosed",
           newFeedback.DateClosed);
        }

        command.Parameters.AddWithValue("Description", newFeedback.Description);
        database.Open();
        var updateCount = command.ExecuteNonQuery();
        database.Close();

        return updateCount;
    }

}