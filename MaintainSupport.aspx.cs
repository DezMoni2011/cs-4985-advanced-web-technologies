﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for MaintainSupport page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 17, 2015
/// </version>
public partial class MaintainSupport : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
    }

    /// <summary>
    /// Handles the ItemDeleted event of the fvSupportDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportDetails_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occured. Please refresh and try again or select another option.<br/><br/>"
                                        + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else
        {
            this.lblErrorMessage.Text = "";
        }

    }

    /// <summary>
    /// Handles the ItemInserted event of the fvSupportDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportDetails_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occured. Please refresh and try again or select another option.<br/><br/>"
                                        + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
        }
        else
        {
            this.lblErrorMessage.Text = "";
        }
    }

    /// <summary>
    /// Handles the ItemUpdated event of the fvSupportDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportDetails_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occured. Please refresh and try again or select another option.<br/><br/>"
                                        + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else
        {
            this.lblErrorMessage.Text = "";
        }
    }
}