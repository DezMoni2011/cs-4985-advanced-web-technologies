﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the FeedbackComplete page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 4, 2015 | Spring
/// </version>
public partial class FeedbackComplete : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        var description = (Description) Session["Description"];

        this.lblCustomerId.Text = description.CustomerId.ToString();
        this.DisplayMessage(description);
    }

    /// <summary>
    /// Displays the message.
    /// </summary>
    /// <param name="description">The description.</param>
    private void DisplayMessage(Description description)
    {
        if (description == null)
        {
            return;
        }

        if (description.Contact)
        {
            this.lblMessage.Text = "We will be contacting you soon with a response to your requests";
        }
        else
        {
            this.lblMessage.Text = "";
        }
    }

    /// <summary>
    /// Handles the Click event of the btnCreateFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnCreateFeedback_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }
}