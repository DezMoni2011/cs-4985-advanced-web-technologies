﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the Default page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// February 28, 2015 | Spring
/// </version>
public partial class Default : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Click event of the btnViewCustomer control.
    /// Sends the user to the CustomerDisplay page.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnViewCustomer_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerDisplay.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnSeeFeedback control.
    /// Sends the user to the CustomerFeedback page.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSeeFeedback_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }
    /// <summary>
    /// Handles the Click event of the btnSeeIncidents control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSeeIncidents_Click(object sender, EventArgs e)
    {
        Response.Redirect("IncidentDisplay.aspx");
    }
}