﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="UpdateFeedback.aspx.cs" Inherits="UpdateFeedback" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Support.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="titleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Update Feedback</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label class="box">Customer: </label>
    <asp:DropDownList ID="ddlCustomers" runat="server" 
        DataSourceID="odsCustomers" 
        DataTextField="Name" 
        DataValueField="CustomerID"
        CssClass="box" AutoPostBack="True">
    </asp:DropDownList>
    <br/>
    <asp:ObjectDataSource ID="odsCustomers" runat="server" 
        SelectMethod="GetCustomersWithFeedback" 
        TypeName="CustomerDatabase"></asp:ObjectDataSource>
    <br />
    <asp:GridView ID="gvUpdatedFeedbacks" runat="server" 
        BackColor="White" 
        BorderColor="#E7E7FF" 
        BorderStyle="None" 
        BorderWidth="1px" 
        CellPadding="3" 
        CssClass="UpdateFeedback" 
        DataSourceID="odsUpdatedFeedbacks" 
        GridLines="Horizontal" 
        OnRowUpdated="gvUpdatedFeedbacks_RowUpdated" AutoGenerateColumns="False" DataKeyNames="DateClosed, Description">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:BoundField DataField="SupportID" HeaderText="Support #" SortExpression="SupportID" ReadOnly="True" />
            <asp:BoundField DataField="SoftwareID" HeaderText="Software ID" SortExpression="SoftwareID" ReadOnly="True" />
            <asp:BoundField DataField="DateOpened" HeaderText="Open Date" SortExpression="DateOpened" ReadOnly="True" />
            <asp:TemplateField HeaderText="Close Date" SortExpression="DateClosed">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("DateClosed") %>' ID="txtCloseDate"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditCloseDate" runat="server" 
                        ControlToValidate="txtCloseDate" 
                        Display="Dynamic" 
                        ErrorMessage="The date closed is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEditCloseDate" runat="server" 
                        ControlToValidate="txtCloseDate" 
                        Display="Dynamic" 
                        ErrorMessage="Date must be in the form mm/dd/yyyy." 
                        ValidationExpression="[0-9]{2}/[0-9]{2}/[0-9]{4} [0-2]{2}:[0-9]{2}:[0-9]{2} AM">*</asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("DateClosed") %>' ID="lblCloseDate"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="Title" HeaderText="Title" ReadOnly="True" SortExpression="Title"></asp:BoundField>

            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEditDescription" runat="server" Text='<%# Bind("Description") %>' 
                        TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditDescription" runat="server" 
                        ControlToValidate="txtEditDescription" Display="Dynamic" 
                        ErrorMessage="A description is required.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    <asp:ObjectDataSource ID="odsUpdatedFeedbacks" runat="server"
        SelectMethod="GetCustomerFeedback" 
        TypeName="FeedbackDatabase" 
        UpdateMethod="UpdateFeedback"
        ConflictDetection="CompareAllValues" 
        OldValuesParameterFormatString="original{0}" 
        OnUpdated="odsUpdatedFeedbacks_Updated" 
        DataObjectTypeName="Feedback">
        <SelectParameters>
            <asp:ControlParameter 
                ControlID="ddlCustomers"
                Name="customerId"
                PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="originalFeedback" Type="Object"></asp:Parameter>
            <asp:Parameter Name="newFeedback" Type="Object"></asp:Parameter>
        </UpdateParameters>
    </asp:ObjectDataSource>
   
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
    <asp:Label ID="lblErrorMessage" CssClass="error" runat="server"></asp:Label>

    <br />

    <asp:ValidationSummary ID="vsUpdateFeedback" runat="server" 
        HeaderText="Please correct the following issues." />
</asp:Content>

